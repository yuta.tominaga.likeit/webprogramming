<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザ削除確認</title>
	<!-- Boostrapにアクセス-->
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
	<body>
		<h2  align="right">
        	<font size="4">  ${userInfo.name} さん
  			<a href="logoutSavlet" class="navbar-link logout-link">ログアウト</a>
  		  	</font>
  		</h2>
	<br>

	<div style="text-align:center">
		<h1>ユーザ削除確認</h1>
	<br>

			<h2><font size="4">ログインID：${user.loginId}</font></h2>
			<h3><font size="3">を本当に削除してもよろしいでしょうか。</font></h3>
		<br>
		<br>

			<input type="button" value="キャンセル" onclick="location.href='UserListServlet'">
			<br>
			<br>
			<form class="form-signin" action="UserDeleteServlet" method="post">
			<input type="hidden" name="id" value="${user.id}">
			<input type="submit" name="OK" value=" OK ">
			</form>

	</div>
	</body>
</html>