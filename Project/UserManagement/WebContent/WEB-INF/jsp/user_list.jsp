<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザ一覧</title>
	<!-- Boostrapにアクセス-->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
    <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <!-- レイアウトカスタマイズ用個別CSS -->

</head>

<body>

	<h2  align="right">
          <font size="4">  ${userInfo.name} さん
  		<a href="logoutSavlet" class="navbar-link logout-link">ログアウト</a>
  		  </font>
  	</h2>

	<br>

	<div style="text-align: center">
		<h1>ユーザ一覧</h1>

		<div style="text-align: right">
			<a href="UserRegistServlet" class="navbar-link regist-link">ユーザ新規登録</a>
		</div>
		<form class="form-signin" action="UserListServlet" method="post">
		<h2>
			<font size="4">ログインID &nbsp;&nbsp;
			<input type="text" name="loginId" style="width: 200px;"></font>
		</h2>
		<h2>
			<font size="4">ユーザ名 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="text" name="name" style="width: 200px;"></font>
		</h2>
		<h2>
			<font size="4">生年月日 &nbsp;&nbsp;
			<input type="date" name="fromDate">
				～
			<input type="date" name="toDate">
			</font>
		</h2>
		<h3>
			<font size="4">
				<input type="submit" value="検索">
			</font>
		</h3>
		</form>
	</div>
	<hr>
	<br>

            <table border="1" align="center">
		<thead>
			<tr>
				<th>ログインID</th>
				<th>ユーザ名</th>
				<th>生年月日</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="user" items="${userList}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>

					<td>
					<c:choose>
					<c:when test="${userInfo.loginId.equals('admin')}">
						<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       	<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                      	<a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
					</c:when>
					<c:otherwise>
						<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
						<c:if test="${userInfo.loginId == user.loginId}">
        	            	<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
    	                </c:if>
					</c:otherwise>
					</c:choose>
					</td>
				</tr>
			</c:forEach>

		</tbody>
	</table>


</body>
</html>