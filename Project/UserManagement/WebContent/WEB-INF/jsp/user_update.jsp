<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー情報更新</title>
</head>
	<body>
		<h2  align="right">
    	      <font size="4">  ${userInfo.name} さん
  			<a href="logoutSavlet" class="navbar-link logout-link">ログアウト</a>
  		  	</font>
  		</h2>

			<div style="text-align:center">
			<h1><b>ユーザ情報更新</b></h1>
			<c:if test="${message != null}" >
	    <div class="alert alert-danger" role="alert">
		<font color="red">${message}</font>
		</div>
		</c:if>

			<br>
				<h2><font size="4"><b>ログインID</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${user.loginId}</font></h2>

		<form class="form-signin" action="UserUpdateServlet" method="post">
				<input type="hidden" name="id" value="${user.id}">
        	<h2><input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワード"></h2>
        	<h2><input type="password" name="checkPassword" id="inputPassword" class="form-control" placeholder="パスワード(確認)"></h2>
        	<h2><input type="text" value="${user.name}" name="name" id="inputLoginId" class="form-control" placeholder="ユーザ名"></h2>
        	<h2><input type="date"  value="${user.birthDate}" name="birth_date" id="inputPassword" class="form-control" placeholder="生年月日"></h2>

		<br>
		<br>
		 	<input type="submit"value="  更新  ">
		</form>
		<br>
				<a href="UserListServlet" class="navbar-link return-link">戻る</a>

			</div>




	</body>
</html>