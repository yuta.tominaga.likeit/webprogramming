<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー情報参照</title>
</head>

	<body>
		<h2  align="right">
          <font size="4">  ${userInfo.name} さん
  		<a href="logoutSavlet" class="navbar-link logout-link">ログアウト</a>
  		  </font>
  		 </h2>

  		<div style="text-align:center">
			<h1><b>ユーザ情報詳細参照</b></h1>
		<br>

			<h2><font size="4"><b>ログインID</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${user.loginId}</font></h2>
			<h2><font size="4"><b>ユーザ名</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${user.name}</font></h2>
			<h2><font size="4"><b>生年月日</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${user.birthDate}</font></h2>
			<h2><font size="4"><b>登録日時</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${user.createDate}</font></h2>
			<h2><font size="4"><b>更新日時</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${user.updateDate}</font></h2>
		<br>
		<br>
	<a href="UserListServlet" class="navbar-link return-link">戻る</a>
		</div>

	</body>
</html>