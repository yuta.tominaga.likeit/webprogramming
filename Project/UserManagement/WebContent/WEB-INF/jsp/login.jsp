<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン画面</title>
	<!-- Boostrapにアクセス-->
	<link href="css/bootstrap.min.css" rel="stylesheet">


</head>

	<body>
		<div style = "text-align:center">
			<br>
	<h1>ログイン画面</h1>
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		<font color="red">${errMsg}</font>
		</div>
	</c:if>

			<br>
		<form class="form-signin" action="LoginServlet" method="post">
		<h2><input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインID"></h2>
        <h2><input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワード"></h2>

		<br>
		<br>
		 <input type="submit"value="  ログイン  ">
		</form>
		 </div>

	</body>
</html>