package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserRegistServlet
 */
@WebServlet("/UserRegistServlet")
public class UserRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあるか確認
		HttpSession session = request.getSession();
		User check = (User) session.getAttribute("userInfo");
		if (check == null) {
			// フォワード
			response.sendRedirect("LoginServlet");
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_regist.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//リクエストパラメータの取得
		String login_id = request.getParameter("login_id");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");

		UserDao registDao = new UserDao();


		//パスワードとパスワード(確認)が不一致だった場合のエラー

		if (!password.equals(checkPassword)) {
			request.setAttribute("message", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_regist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//ログインIDがすでに使用されている場合のエラー
		UserDao userDao = new UserDao();
		boolean id = userDao.findByLoginId(login_id);

		if (id) {
			request.setAttribute("message", "入力された内容は正しくありません");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_regist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if (login_id.equals("") || name.equals("") || birth_date.equals("") || password.equals("")) {
			request.setAttribute("message", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_regist.jsp");
			dispatcher.forward(request, response);
			return;

		}
		registDao.userRegist(login_id, name, birth_date, password);
		response.sendRedirect("UserListServlet");
	}

}
