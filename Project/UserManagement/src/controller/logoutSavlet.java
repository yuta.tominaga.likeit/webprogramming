package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;

/**
 * Servlet implementation class logoutSavlet
 */
@WebServlet("/logoutSavlet")
public class logoutSavlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public logoutSavlet() {
        super();
    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//セッションがあるか確認
		User check = (User) session.getAttribute("userInfo");
		if (check == null) {
			// フォワード
			response.sendRedirect("LoginServlet");
			return;
		}

		// ログイン時に保存したセッション内のユーザ情報を削除
		session.removeAttribute("userInfo");

		// ログインのサーブレットにリダイレクト
		response.sendRedirect("LoginServlet");

	}
}
