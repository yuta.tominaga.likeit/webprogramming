package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッションがあるかの確認
		HttpSession session = request.getSession();
		User check = (User) session.getAttribute("userInfo");
		if (check == null) {
			// フォワード
			response.sendRedirect("LoginServlet");
			return;
		}

		//URLからIDの取得
		String id = request.getParameter("id");
		//コンソールにIDの表示
		System.out.println(id);
		//Idに紐づく情報を更新するためUserDaoへ
		UserDao userDao = new UserDao();
		User update = userDao.findId(id);

		request.setAttribute("user", update);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");
		String id = request.getParameter("id");

		UserDao updateDao = new UserDao();

		//パスワードとパスワード(確認)が不一致だった場合のエラー
		if (!password.equals(checkPassword)) {
			request.setAttribute("message", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//nameとbirth_dateが空欄だった場合エラーの表示
		if (name.equals("") || birth_date.equals("")){
			request.setAttribute("message", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
			dispatcher.forward(request, response);
			return;
		}

		updateDao.userUpdate(name, birth_date, password,id);
		response.sendRedirect("UserListServlet");


	}

}
